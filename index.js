const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const swaggerUI = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
PUBLIC_DIRECTORY = path.join(__dirname, 'public');
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// import controllers
const carscontrollers = require("./app/controllers/api/v1/carscontrollers");
const userController = require("./app/controllers/api/v1/user");

// authentication - routes
app.post("/user/register", userController.register);
app.post("/user/login", userController.login);
app.post("/user/registerAdmin", userController.authorizeSuperAdmin, userController.registerAdmin);
app.delete("/user/delete/:id", userController.authorize, userController.deleteUser);
app.delete("/admin/delete/:id", userController.authorizeSuperAdmin, userController.deleteAdmin);
app.get("/user/data", userController.authorize, userController.authorizeUser);

// cars data - routes
app.get("/cars", carscontrollers.getCars);
app.post("/cars/create", userController.authorizeAdmin, carscontrollers.createCars);
app.put("/cars/update/:id", userController.authorizeAdmin, carscontrollers.editCarsById);
app.delete("/cars/delete/:id", carscontrollers.deleteCarsById);

// API Documentation
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.listen(PORT, () => {console.log(`Server is now running on http://localhost:${PORT}`);});
  