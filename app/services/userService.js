const userRepository = require("../repositories/userRepository");
const { encryptPassword, comparePassword, createToken } = require("../controller/api/v1/authentication");
const { userRole } = require("../models");
const jwt = require("jsonwebtoken");

module.exports = {

    async create(requestBody) {
        const firstName = requestBody.body.firstName;
        const lastName = requestBody.body.lastName;
        const email = requestBody.body.email;
        const password = requestBody.body.password;
        const encryptedPassword = await encryptPassword(password);
        return userRepository.create({firstName, lastName, email, encryptedPassword, userRoleID: 3});
    },


    async createAdmin(requestBody) {
        const firstName = requestBody.body.firstName;
        const lastName = requestBody.body.lastName;
        const email = requestBody.body.email;
        const password = requestBody.body.password;
        const encryptedPassword = await encryptPassword(password);
        return userRepository.create({firstName, lastName, email, encryptedPassword, userRoleID: 2});
    },


    async createSuperAdmin(requestBody) {
        const firstName = requestBody.body.firstName;
        const lastName = requestBody.body.lastName;
        const email = requestBody.body.email;
        const password = requestBody.body.password;
        const encryptedPassword = await encryptPassword(password);
        return userRepository.create({firstName, lastName, email, encryptedPassword, userRoleID: 1});
    },


    async checkUser(requestBody) {
        const email = requestBody.body.email;
        const password = requestBody.body.password;
        const user = await userRepository.findOne({where:{email}, include: userRole});
        const isPasswordCorrect = await comparePassword(user.encryptedPassword, password);
        const token = createToken({ id: user.id, email: user.email, firstName: user.firstName, lastName: user.lastName, role: user.userRoleID});

        if (!user) {
            res.status(404).json({
                status:'ERROR',
                data: null,
                message: 'User not found'
            })
            return;
        }

        if(!isPasswordCorrect) {
            res.status(404).json({
                status:'ERROR',
                data: null,
                message: 'Password is incorrect'
            })
            return;
        }

        return { 
            data: user,
            token,
            message: 'User found!'
        }
    },


    async whoAmI(req, res) {
        res.status(200).json(req.user)
    },


    async authorize(req, res) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Secret");
            const id = tokenPayload.id;
            requestUser = await userRepository.findByPk(tokenPayload.id);
            user = await userRepository.findOne({where:{id}, include: userRole})

            if(requestUser.userRoleID){
                return{
                    data: requestUser,
                    user
                }
            }

        } catch(err) {
            res.status(401).json({
                status:'ERROR',
                data: null,
                message: 'Unauthorized'
            })
        }
    },

    async authorizeUser(req, res) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Secret");
            const id = tokenPayload.id;
            requestUser = await userRepository.findByPk(tokenPayload.id);
            user = await userRepository.findOne({where:{id}, include: userRole})

            if(requestUser.userRoleID){
                return{
                    data: requestUser,
                    user
                }
            }
            
        } catch(err) {
            res.status(401).json({
                status:'ERROR',
                data: null,
                message: 'Unauthorized'
            })
        }
    },

    async authorizeAdmin(req, res) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Secret");
            const id = tokenPayload.id;
            requestUser = await userRepository.findByPk(tokenPayload.id);
            user = await userRepository.findOne({where:{id}, include: userRole})

            if(requestUser.userRoleID == 1 || requestUser.userRoleID == 2){
                return{
                    data: requestUser,
                    user
                }
            }
        } catch(err) {
            res.status(401).json({
                status:'ERROR',
                data: null,
                message: 'Unauthorized'
            })
        }
    },

    async authorizeSuperAdmin(req, res) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const tokenPayload = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Secret");
            requestUser = await userRepository.findByPk(tokenPayload.id);
            
            if(requestUser.userRoleID == 1){
                return{
                    data: requestUser,
                }
            }
            
        } catch(err) {
            res.status(401).json({
                status:'ERROR',
                data: null,
                message: 'Unauthorized'
            })
        }
    },


    async deleteUser(id) {
        return userRepository.deleteUser(id);
    },


    async deleteAdmin(id){
        return userRepository.deleteAdmin(id);
    },

}