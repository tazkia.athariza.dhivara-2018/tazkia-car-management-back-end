const { user } = require("../models");
const { Op } = require("sequelize");

module.exports = {

  create(requestBody) {
    return user.create(requestBody);
  },

  findOne(email){
    return user.findAll(email)
  },

  findByPk(id){
    return user.findByPk(id)
  },

  deleteUser(id){
    return user.destroy({
      where: { 
        [Op.and]: [
          { id },
          { userRoleID : 2 }
        ]
       },
    })
  },

  deleteAdmin(id){
    return user.destroy({
      where: { 
        [Op.and]: [
          { id },
          { userRoleID : 3 }
        ]
       },
    })
  },

};
