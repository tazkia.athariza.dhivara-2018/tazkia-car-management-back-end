const user = require("./user");
const auth = require("./authentication")

module.exports = { user, auth }