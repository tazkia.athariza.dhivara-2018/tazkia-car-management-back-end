const userService = require("../../../services/userService");

module.exports = {

    async register (req, res) {
        try {
            const user = userService.create(req);
            res.status(200).json({
                status: 'OK',
                data: user,
                message: 'User was successfully created!'
            })
        } catch (err){
            res.status(500).json({
                status:'ERROR',
                data: null,
                message: 'Failed to create new user!'
            })
        }
    },

    async registerAdmin (req, res) {
        try {
            const user = userService.createAdmin(req);
            res.status(200).json({
                status: 'OK',
                data: user,
                message: 'Admin was successfully created!'
            })
        } catch (err){
            res.status(500).json({
                status:'ERROR',
                data: null,
                message: 'Failed to create new admin!'
            })
        }
    },

    async registerSuperAdmin (req, res) {
        try {
            const user = userService.createSuperAdmin(req);
            res.status(200).json({
                status: 'OK',
                data: user,
                message: 'SuperAdmin was successfully created!'
            })
        } catch (err){
            res.status(500).json({
                status:'ERROR',
                data: null,
                message: 'Failed to create new admin!'
            })
        }
    },

    async login (req, res) {
        try {
            const result = userService.checkUser(req);
            const { data, message, token } = result;
            if (!data) {
                res.status(401).json({
                    status:'ERROR',
                    data: null,
                    message: 'Error'
                })
                return;
            }
            res.status(200).json({
                status: 'OK',
                data,
                token,
                message
            })
            
        } catch (err){
            res.status(400).json({
                status:'ERROR',
                data: null,
                message: err
            })
        }
    },

    async authorize (req, res, next) {
        try{
            const result = await userService.authorizeUser(req);
            if(!result.data){
                res.json({
                    message : 'Unauthorized'
                });
            }
            req.nameAuthorize = result.data.firstName;
            req.roleAuthorize = res.data.userRoleID;
            req.roleID = result.data.id;
            req.result = result;
            next();
        } catch {
            res.status(404).json({
                message: 'ERROR'
            });
        }
    },

    async authorizeUser(req, res){
        try{
            const result = await req.result;
            res.status(200).json({
                AccessedBy: result.data.firstName,
                Role: result.user.userRole.name
            })
        }catch{
            res.status(404).json({
                message: 'ERROR',
            })
        }
    },

    async authorizeAdmin(req, res, next){
        try{
            const result = await userService.authorizeAdmin(req);
            if(!result.data){
                res.json({
                    message : 'Unauthorized'
                });
            }
            req.nameAuthorizeAdmin = result.data.firstName;
            req.roleAuthorizeAdmin = result.data.userRoleID;
            req.roleID = result.data.id;
            req.result = result;
            next();
        }catch{
            res.status(404).json({
                message: 'ERROR'
            })
        }
    },

    async authorizeSuperAdmin(req, res, next){
        try{
            const result = await userService.authorizeSuperAdmin(req);
            if(!result.data){
                res.json({
                    message : 'Unauthorized'
                });
            }
            req.nameAuthorizeSuperAdmin = result.data.firstName;
            req.roleAuthorizeSuperAdmin = result.data.userRoleID;
            req.roleID = result.data.id;
            next();
        }catch{
            res.status(404).json({
                message: 'ERROR'
            });
        }
    },

    async deleteUser (req, res) {
        try {
            const result = await userService.deleteUser(req.params.id);
            res.status(200).json({
            status: 'OK',
            data: result,
            message: 'User successfully deleted'
        })
        } catch (err){
            res.status(404).json({
                status:'ERROR',
                data: null,
                message: 'Failed to delete user'
            })
        }
    },


    async deleteAdmin (req, res) {
        try {
            const result = await userService.deleteAdmin(req.params.id);
            res.status(200).json({
            status: 'OK',
            data: result,
            message: 'Admin successfully deleted'
        })
        } catch (err){
            res.status(404).json({
                status:'ERROR',
                data: null,
                message: 'Failed to delete admin'
            })
        }
    },

}