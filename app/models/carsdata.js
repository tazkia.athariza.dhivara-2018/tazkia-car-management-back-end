'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class carsdata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  carsdata.init({
    carType: DataTypes.STRING,
    carPrice: DataTypes.INTEGER,
    carImage: DataTypes.STRING,
    carSize: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'carsdata',
  });
  return carsdata;
};