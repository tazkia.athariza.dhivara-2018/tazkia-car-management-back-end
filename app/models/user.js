'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.userRole, {
        foreignKey: 'userRoleID',
        as: 'userRole'
      })
    }
  }

  user.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    encryptedPassword: DataTypes.STRING,
    userRoleID: {
      type: DataTypes.INTEGER,
      references: {
        model: 'userRole',
        key: 'id'
      }
    }
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};