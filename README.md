# Tazkia - Car Management API - Challenge 6 
"Car Management API" was created for academic purpose, called Challenge Chapter 6 in Binar Academy - Fullstack Web Course. The aims of this challenge are to implement CRUD, do authentication using token session, do authorization, and make Open API Documentation using swagger, etc.

## About
This project is containing three main tables, those are carsdata table (for managing cars data), user table, and userRole table. All tables are related to each other directly or indirectly. The logic in this system was written following by design service repository pattern.

## Getting Started
This project is using several third-party modules including express, sequalize, bcryptjs, swagger, jsonwebtoken, etc. For database, this website is using postgreSQL. So if you're about to run this program, you need to install those modules first. After that, run 'npm install' and 'npm start' in your terminal. Before doing migration, you also need to run 'sequelize init' first. This repository doesn't have any frontend page (user interface), so for testing you would need 'postman' aplication. Run the program from your terminal by using this command : 'node index.js'.

## Version history
https://gitlab.com/tazkia.athariza.dhivara-2018/tazkia-car-management-back-end/-/commits/main

## Author
Tazkia Athariza D