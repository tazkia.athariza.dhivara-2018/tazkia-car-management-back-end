'use strict';
const { encryptPassword } = require("../../app/controllers/api/v1/authentication")
// const {encryptPassword} = require("../app/controllers/api/v1/authentication")

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('users', [
     {
      firstName: "SuperAdmin",
      lastName: "taz",
      email: "superAdmin@gmail.com",
      encryptedPassword: await encryptPassword('12345'),
      userRoleID: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: "admin",
      lastName: "taz",
      email: "admin@gmail.com",
      encryptedPassword: await encryptPassword('12345'),
      userRoleID: 2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: "member",
      lastName: "taz",
      email: "member@gmail.com",
      encryptedPassword: await encryptPassword('12345'),
      userRoleID: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    },
  ])
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {})
  }
};
